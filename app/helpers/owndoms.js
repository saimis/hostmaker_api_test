const db = require('../../config/db')
const https = require('https')
const request = require('request')
const settings = require("../../config/settings")

exports.airbnbIdExists = (airbnb_id) => {
  var options = {
    url: `https://www.airbnb.co.uk/rooms/${airbnb_id}`,
    headers: { 'User-Agent': 'hostmaker_request' },
    followRedirect: false
  }

  return new Promise((resolve, reject) => {
    request(options, (error, response, body) => {
      response.statusCode == 200 ? resolve(true) : reject('ID does not exist on Airbnb')
    })
  })
}

exports.uniqueAirbnbId = (airbnb_id, owndom_id) => {
  return new Promise((resolve, reject) => {
    let owndom_query = 'select * from properties where airbnb_id = ?'

    // if record being updated, ignore it's own airbnb ID
    if (owndom_id) {
      owndom_query = ` ${owndom_query} AND id != ${owndom_id}`
    }

    db.query(owndom_query, [airbnb_id], (error, result, fields) => {
      result.length > 0 ? reject('Airbnb ID is not unique') : resolve(true)
    })
  })
}
