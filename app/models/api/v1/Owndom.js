const db            = require('../../../../config/db')
const schemas       = require('../../../../config/schemas')
const OwndomsHelper = require('../../../helpers/owndoms')
const Joi           = require('joi')

exports.Owndom = class {
  constructor(data) {
    this.data = data
  }

  static all() {
    return new Promise((resolve, reject) => {
      db.query("select * from properties", (error, result, fields) => {
        error ? reject(error) : resolve(result)
      })
    })
  }

  static find(id, callback) {
    return new Promise((resolve, reject) => {
      db.query("select * from properties where id= ?", [id], (error, result, fields) => {
        error ? reject(error) : resolve(result)
      })
    })
  }

  create() {
    return new Promise((resolve, reject) => {
      const validation = Joi.validate(this.data, Joi.object().keys(schemas.owndom))

      if (validation.error) {
        reject(validation.error)
      }

      // checking if id is not already used and if it exists in Airbnb servers
      Promise.all([
        OwndomsHelper.uniqueAirbnbId(this.data.airbnb_id),
        OwndomsHelper.airbnbIdExists(this.data.airbnb_id)
      ]).then(() => {

        db.query('INSERT INTO properties SET ?', this.data, (error, result, fields) => {
          error ? reject(error) : resolve(result)
        })
      }).catch((error) => { reject(error) })
    })
  }

  async update(id) {
    let attribute_validity = {}

    // go through all updated keys to check if they're valid, hacky Joi
    Object.keys(this.data).forEach((key, value) => {
      let result = Joi.validate(this.data[key], schemas.owndom[key])

      if (result.error) {
        attribute_validity[key] = result.error.details[0].message.replace(/"value"/, '')
      }
    })

    let id_valid = []

    if (this.data.airbnb_id && Object.keys(attribute_validity).length == 0) {
      // checking if id is not already used and if it exists in Airbnb servers
      try {
        id_valid = await Promise.all([
          OwndomsHelper.uniqueAirbnbId(this.data.airbnb_id, id),
          OwndomsHelper.airbnbIdExists(this.data.airbnb_id)
        ]).catch((error) => { return [error] })
      } catch(error) {
        throw new Error(error)
      }
    }

    return new Promise((resolve, reject) => {
      if (this.data.airbnb_id && !id_valid.every(e => e == true)) {
        reject(id_valid.join(''))
      }

      if (Object.keys(attribute_validity).length > 0) {
        reject(attribute_validity)
      }

      db.query(`UPDATE properties SET ? WHERE id = ${id}`, this.data, (error, result, fields) => {
        error ? reject(error) : resolve(result)
      })
    })
  }

  static destroy(id) {
    return new Promise((resolve, reject) => {
      db.query("delete from properties where id= ?", [id], (error, result, fields) => {
        error ? reject(error) : resolve(result)
      })
    })
  }
}
