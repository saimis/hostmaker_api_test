const { Owndom } = require('../../../models/api/v1/Owndom')

exports.OwndomsController = class {
  all_owndoms (req, res) {
    Owndom.all()
      .then(result => res.status(200).json({ result: 'success', data: result }))
      .catch(error => res.status(200).json({ result: 'error', reason: error }))
  }

  owndom_by_id (req, res) {
     Owndom.find(req.params.id)
      .then(result => res.status(200).json({ result: 'success', data: result }))
      .catch(error => res.status(200).json({ result: 'error', reason: error }))
  }

  create_owndom (req, res) {
    const owndom = new Owndom(req.body)

    owndom.create()
      .then(result => res.status(200).json({ result: 'success', data: { property_id: result.insertId }}))
      .catch(error => res.status(200).json({ result: 'error', reason: error }))
  }

  update_owndom (req, res) {
    const owndom = new Owndom(req.body)

    owndom.update(req.params.id)
      .then(result => res.status(200).json({ result: 'success', data: { updated: result.affectedRows }}))
      .catch(error => res.status(200).json({ result: 'error', reason: error }))
  }

  delete_owndom (req, res) {
    Owndom.destroy(req.params.id)
      .then(result => res.status(200).json({ result: 'success', data: { deleted: result.affectedRows } }))
      .catch(error => res.status(200).json({ result: 'error', reason: error }))
  }
}
