const express = require('express')
const api_v1  = require('../controllers/api/v1')

module.exports = function(app) {
  app.get('/', (req, res) => {
    res.send('Hostmaker properties API is at /api/v1/owndoms')
  })

  const apiRouter = express.Router({ mergeParams: true })

  apiRouter.route('/owndoms/new')
    .get(api_v1.owndoms.create_owndom)
    .post(api_v1.owndoms.create_owndom)

  apiRouter.route('/owndoms/:id/update')
    .get(api_v1.owndoms.update_owndom)
    .post(api_v1.owndoms.update_owndom)

  apiRouter.route('/owndoms/:id/delete')
    .get(api_v1.owndoms.delete_owndom)
    .post(api_v1.owndoms.delete_owndom)

  apiRouter.route('/owndoms/:id')
    .get(api_v1.owndoms.owndom_by_id)
    .post(api_v1.owndoms.owndom_by_id)

  apiRouter.route('/owndoms')
    .get(api_v1.owndoms.all_owndoms)
    .post(api_v1.owndoms.all_owndoms)

  app.use('/api/v1/', apiRouter)
}
