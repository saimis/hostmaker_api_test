### Installation

First run `yarn`

Create database, ex: `hostmaker_api` (please update `config/db.js` with correct information)

Run `mysql -u <username> -p<password> hostmaker_api < config/database.sql`


### Start application

Run `node server.js` to start server

API is now available at http://localhost:8000/api/v1/owndoms


### Calling API

I used cURL to post to API.

Some queries:


to update
`curl --data "owner_name=Johnatan&post_code=1234" http://localhost:8000/api/v1/owndoms/1/update`

to insert
`curl --data "owner_name=Johnathan&airbnb_id=19996417&city=London&country=UK&county=Greenwich&post_code=9F G2&address_1=Green st. 1&bedrooms=1&latitude=51.4565468&longitude=-0.4565468&bathrooms=2" http://localhost:8000/api/v1/owndoms/new`



Other calls can be made from browser:

All records
`http://localhost:8000/api/v1/owndoms/`

Single record
`http://localhost:8000/api/v1/owndoms/1`

Delete record
`http://localhost:8000/api/v1/owndoms/1/delete`

### Misc
Tested on Node.js 8.4.0
