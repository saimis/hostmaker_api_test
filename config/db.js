var mysql = require('mysql')

var connection = mysql.createPool({
  host     : 'localhost',
  user     : 'root',
  password : 'toor',
  database : 'hostmaker_api'
})

module.exports = connection
