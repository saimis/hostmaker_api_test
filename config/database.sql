--Before running this file create database CREATE DATABASE hostmaker_api CHARACTER SET utf8 COLLATE utf8_general_ci;


CREATE TABLE properties (
  id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  owner_name VARCHAR(255) NOT NULL,
  airbnb_id INT(11),
  city VARCHAR(255) NOT NULL,
  county VARCHAR(255) NOT NULL,
  country VARCHAR(2),
  post_code VARCHAR(255) NOT NULL,
  address_1 VARCHAR(255) NOT NULL,
  address_2 VARCHAR(255) NOT NULL,
  bedrooms INT(11),
  latitude float,
  longitude float,
  bathrooms DECIMAL(2,1)
);

CREATE TABLE versions (
  id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  item_id INT(11) NOT NULL,
  event VARCHAR(255) NOT NULL,
  object LONGTEXT,
  created_at DATETIME
);


INSERT INTO `properties` (`id`, `owner_name`, `airbnb_id`, `city`, `country`, `county`, `post_code`, `address_1`, `address_2`, `bedrooms`, `latitude`, `longitude`, `bathrooms`)
  VALUES (1, 'John', 8359093, 'London', 'GB', 'London', '00113', 'Greenwitch st. 2', '', '1', '55.137747', '0.518786', '1');

INSERT INTO `properties` (`id`, `owner_name`, `airbnb_id`, `city`, `country`, `county`, `post_code`, `address_1`, `address_2`, `bedrooms`, `latitude`, `longitude`, `bathrooms`)
  VALUES (2, 'John', 19652995, 'Munchen', 'DE', 'Bavaria', '001-BV', 'Hauzenberger st. 2', '', '1', '48.137747', '11.518786', '1');

DELIMITER //

CREATE TRIGGER properties_after_insert AFTER INSERT ON properties FOR EACH ROW BEGIN
  INSERT INTO versions (item_id, event, object, created_at) VALUES (NEW.id, 'insert',
    CONCAT('{ id: ', NEW.id,' owner_name: ', NEW.owner_name,' airbnb_id: ', NEW.airbnb_id,' city: ', NEW.city,' county: ', NEW.county, ' country: ', NEW.country,' post_code: ', NEW.post_code,' address_1: ', NEW.address_1,' address_2: ', NEW.address_2,' bedrooms: ', NEW.bedrooms,' latitude: ', NEW.latitude,' longitude: ', NEW.longitude,' bathrooms: ', NEW.bathrooms, ' }'), NOW());
END//

CREATE TRIGGER properties_after_update AFTER UPDATE ON properties FOR EACH ROW BEGIN
  INSERT INTO versions (item_id, event, object, created_at) VALUES (NEW.id, 'update',
    CONCAT('{ id: ', NEW.id,' owner_name: ', NEW.owner_name,' airbnb_id: ', NEW.airbnb_id,' city: ', NEW.city,' county: ', NEW.county, ' country: ', NEW.country,' post_code: ', NEW.post_code,' address_1: ', NEW.address_1,' address_2: ', NEW.address_2,' bedrooms: ', NEW.bedrooms,' latitude: ', NEW.latitude,' longitude: ', NEW.longitude,' bathrooms: ', NEW.bathrooms, ' }'), NOW());
END//

CREATE TRIGGER properties_after_delete AFTER DELETE ON properties FOR EACH ROW BEGIN
  INSERT INTO versions (item_id, event, object, created_at) VALUES (OLD.id, 'delete',
    CONCAT('{ id: ', OLD.id,' owner_name: ', OLD.owner_name,' airbnb_id: ', OLD.airbnb_id,' city: ', OLD.city,' county: ', OLD.county, ' country: ', OLD.country,' post_code: ', OLD.post_code,' address_1: ', OLD.address_1,' address_2: ', OLD.address_2,' bedrooms: ', OLD.bedrooms,' latitude: ', OLD.latitude,' longitude: ', OLD.longitude,' bathrooms: ', OLD.bathrooms, ' }'), NOW());
END//

DELIMITER ;
