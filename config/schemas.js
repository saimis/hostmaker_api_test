const Joi = require('joi')
const schemas = {
  owndom: {
    owner_name: Joi.string().alphanum().min(3).required(),
    airbnb_id:  Joi.number().integer().required(),
    city:       Joi.valid(['Paris', 'Rome', 'London', 'Barcelona']).required(),
    county:     Joi.string().alphanum().required(),
    country:    Joi.valid(['FR', 'IT', 'UK', 'ES']).required(),
    post_code:  Joi.string().required(),
    address_1:  Joi.string().required(),
    address_2:  Joi.string(),
    bedrooms:   Joi.number().integer().required(),
    latitude:   Joi.number().required(),
    longitude:  Joi.number().required(),
    bathrooms:  Joi.number().required(),
  }
}

module.exports = schemas
