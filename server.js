const express = require('express')
const parser  = require('body-parser')
const http    = require('http')
let app       = express()

app.use(parser.urlencoded({ extended: true }))
app.use(parser.json())
app.set('port', 8000)

require('./app/routes')(app)

http.createServer(app).listen(app.get('port'), () => {
  console.log(`Api is available at http://localhost:${app.get('port')}/api/v1/owndoms`)
})
